# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from is_safe_url import PY2, is_safe_url


# Based on Django d22b90b4eabc1fe9b7b35aada441e0edf5ebd6d8
class IsSafeURLTests(unittest.TestCase):
    def test_bad_urls(self):
        bad_urls = (
            "http://example.com",
            "http:///example.com",
            "https://example.com",
            "ftp://example.com",
            r"\\example.com",
            r"\\\example.com",
            r"/\\/example.com",
            r"\\\example.com",
            r"\\example.com",
            r"\\//example.com",
            r"/\/example.com",
            r"\/example.com",
            r"/\example.com",
            "http:///example.com",
            r"http:/\//example.com",
            r"http:\/example.com",
            r"http:/\example.com",
            'javascript:alert("XSS")',
            "\njavascript:alert(x)",
            "\x08//example.com",
            r"http://otherserver\@example.com",
            r"http:\\testserver\@example.com",
            r"http://testserver\me:pass@example.com",
            r"http://testserver\@example.com",
            r"http:\\testserver\confirm\me@example.com",
            "http:999999999",
            "ftp:9999999999",
            "\n",
            "http://[2001:cdba:0000:0000:0000:0000:3257:9652/",
            "http://2001:cdba:0000:0000:0000:0000:3257:9652]/",
        )
        for bad_url in bad_urls:
            self.assertIs(
                is_safe_url(bad_url, allowed_hosts={"testserver", "testserver2"}), False
            )

    def test_good_urls(self):
        good_urls = (
            "/view/?param=http://example.com",
            "/view/?param=https://example.com",
            "/view?param=ftp://example.com",
            "view/?param=//example.com",
            "https://testserver/",
            "HTTPS://testserver/",
            "//testserver/",
            "http://testserver/confirm?email=me@example.com",
            "/url%20with%20spaces/",
            "path/http:2222222222",
        )
        for good_url in good_urls:
            self.assertIs(
                is_safe_url(good_url, allowed_hosts={"otherserver", "testserver"}), True
            )

    @unittest.skipUnless(PY2, "Only run on Python 2")
    def test_python2_binary(self):
        # Check binary URLs, regression tests for #26308
        self.assertIs(
            is_safe_url(b"https://testserver/", allowed_hosts={"testserver"}),
            True,
            "binary URLs should be allowed on Python 2",
        )
        self.assertIs(
            is_safe_url(b"\x08//example.com", allowed_hosts={"testserver"}), False
        )
        self.assertIs(
            is_safe_url("àview/".encode("utf-8"), allowed_hosts={"testserver"}), True
        )
        self.assertIs(
            is_safe_url("àview".encode("latin-1"), allowed_hosts={"testserver"}), False
        )

    def test_basic_auth(self):
        # Valid basic auth credentials are allowed.
        self.assertIs(
            is_safe_url(
                r"http://user:pass@testserver/", allowed_hosts={"user:pass@testserver"}
            ),
            True,
        )

    def test_no_allowed_hosts(self):
        # A path without host is allowed.
        self.assertIs(is_safe_url("/confirm/me@example.com", allowed_hosts=None), True)
        # Basic auth without host is not allowed.
        self.assertIs(
            is_safe_url(r"http://testserver\@example.com", allowed_hosts=None), False
        )

    def test_allowed_hosts_str(self):
        self.assertIs(
            is_safe_url("http://good.com/good", allowed_hosts="good.com"), True
        )
        self.assertIs(
            is_safe_url("http://good.co/evil", allowed_hosts="good.com"), False
        )

    def test_secure_param_https_urls(self):
        secure_urls = (
            "https://example.com/p",
            "HTTPS://example.com/p",
            "/view/?param=http://example.com",
        )
        for url in secure_urls:
            self.assertIs(
                is_safe_url(url, allowed_hosts={"example.com"}, require_https=True),
                True,
            )

    def test_secure_param_non_https_urls(self):
        insecure_urls = (
            "http://example.com/p",
            "ftp://example.com/p",
            "//example.com/p",
        )
        for url in insecure_urls:
            self.assertIs(
                is_safe_url(url, allowed_hosts={"example.com"}, require_https=True),
                False,
            )
